all: paper.pdf pres.pdf

%.pdf : %.tex
	latexmk -pdf $<


clean:
	rm -f *.aux *.bbl *.blg *.fdb_latexmk *.fls *.log *.out *~ *.ptb
