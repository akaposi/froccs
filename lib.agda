module lib where

data _≡_ {A : Set}(a : A) : A → Set where
  refl : a ≡ a

infix 1 _≡_

ap : {A B : Set}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
ap f refl = refl

ap2 : {A B C : Set}(f : A → B → C){a a' : A} → a ≡ a' → {b b' : B} → b ≡ b' →  f a b ≡ f a' b'
ap2 f refl refl = refl

_⁻¹ : {A : Set}{a a' : A} → a ≡ a' → a' ≡ a
refl ⁻¹ = refl

_◾_ : {A : Set}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
refl ◾ refl = refl

data ℕ : Set where
  zero : ℕ
  suc : ℕ → ℕ

{-# BUILTIN NATURAL ℕ #-}

_+_ : ℕ → ℕ → ℕ
zero + m = m
suc n + m = suc (n + m)

infix 4 _+_

record Σ (A : Set)(B : A → Set) : Set where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁

open Σ public

_×_ : Set → Set → Set
A × B = Σ A λ _ → B

infix 2 _,_

+ass : {m n o : ℕ} → m + (n + o) ≡ (m + n) + o
+ass {zero} = refl
+ass {suc m} = ap suc (+ass {m})

+idl : {m : ℕ} → 0 + m ≡ m
+idl = refl

+idr : {m : ℕ} → m + 0 ≡ m
+idr {zero} = refl
+idr {suc m} = ap suc (+idr {m})

lem+comm : {m n : ℕ} → suc (n + m) ≡ n + suc m
lem+comm {n = zero} = refl
lem+comm {n = suc n} = ap suc (lem+comm {n = n})

+comm : {m n : ℕ} → m + n ≡ n + m
+comm {zero} = +idr ⁻¹
+comm {suc m}{n} = ap suc (+comm {m}) ◾ lem+comm {m}{n}

data _⊎_ (A B : Set) : Set where
  inj₁ : A → A ⊎ B
  inj₂ : B → A ⊎ B

infix 1 _⊎_
