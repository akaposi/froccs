module froccs where

open import lib renaming (_+_ to _+'_)

-- szintaxis

data Deci : Set where
  bor szo : Deci

data Prefroccs : Set where
  deci : Deci → Prefroccs
  ε : Prefroccs
  _+_ : Prefroccs → Prefroccs → Prefroccs

infix 3 _+_

-- tipusrendszer

data Ty : Set where
  Vanbor Vanszo Froccs : Ty

data _∶_ : Prefroccs → Ty → Set where
  b  : deci bor   ∶ Vanbor
  s  : deci szo ∶ Vanszo
  bs : ∀{p₁ p₂} → p₁ ∶ Vanbor   → p₂ ∶ Vanszo → p₁ + p₂ ∶ Froccs
  sb : ∀{p₁ p₂} → p₁ ∶ Vanszo → p₂ ∶ Vanbor   → p₁ + p₂ ∶ Froccs
  _+p  : ∀{p₁ p₂ A} → p₁ ∶ A → p₁ + p₂ ∶ A
  p+_  : ∀{p₁ p₂ A} → p₂ ∶ A → p₁ + p₂ ∶ A

infix 2 _∶_

lemma4b : ∀{p} → p ∶ Froccs → p ∶ Vanbor
lemma4b (bs t₁ t₂) = t₁ +p
lemma4b (sb t₁ t₂) = p+ t₂
lemma4b (t +p) = lemma4b t +p
lemma4b (p+ t) = p+ lemma4b t

lemma4s : ∀{p} → p ∶ Froccs → p ∶ Vanszo
lemma4s (bs t₁ t₂) = p+ t₂
lemma4s (sb t₁ t₂) = t₁ +p
lemma4s (t +p) = lemma4s t +p
lemma4s (p+ t) = p+ lemma4s t

-- denotacios szemantika

⟦_⟧ : Prefroccs → ℕ × ℕ
⟦ deci bor ⟧ = 1 , 0
⟦ deci szo ⟧ = 0 , 1
⟦ ε ⟧ = 0 , 0
⟦ p₁ + p₂ ⟧ = proj₁ ⟦ p₁ ⟧ +' proj₁ ⟦ p₂ ⟧
            , proj₂ ⟦ p₁ ⟧ +' proj₂ ⟦ p₂ ⟧

pelda1 : ⟦ (deci bor + deci szo) + ε ⟧ ≡ 1 , 1
pelda1 = refl

pelda2 : ⟦ deci bor + (deci bor + deci szo) ⟧ ≡ 2 , 1
pelda2 = refl

pelda3 : ⟦ deci bor + (deci szo + (deci bor + ε)) ⟧ ≡ 2 , 1
pelda3 = refl

pelda4 : ⟦ deci szo + (deci bor + deci bor) ⟧ ≡ 2 , 1
pelda4 = refl

lemma5ass : ∀{p₁ p₂ p₃} → ⟦ p₁ + (p₂ + p₃) ⟧ ≡ ⟦ (p₁ + p₂) + p₃ ⟧
lemma5ass {p₁}{p₂}{p₃} = ap2 _,_ (+ass {proj₁ ⟦ p₁ ⟧}{proj₁ ⟦ p₂ ⟧}{proj₁ ⟦ p₃ ⟧})
                                 (+ass {proj₂ ⟦ p₁ ⟧}{proj₂ ⟦ p₂ ⟧}{proj₂ ⟦ p₃ ⟧})

lemma5idl : ∀{p} → ⟦ ε + p ⟧ ≡ ⟦ p ⟧
lemma5idl = refl

lemma5idr : ∀{p} → ⟦ p + ε ⟧ ≡ ⟦ p ⟧
lemma5idr {p} = ap2 _,_ +idr +idr

lemma5comm : ∀{p₁ p₂} → ⟦ p₁ + p₂ ⟧ ≡ ⟦ p₂ + p₁ ⟧
lemma5comm {p₁}{p₂} = ap2 _,_ (+comm {proj₁ ⟦ p₁ ⟧}{proj₁ ⟦ p₂ ⟧})
                              (+comm {proj₂ ⟦ p₁ ⟧}{proj₂ ⟦ p₂ ⟧})

-- operacios szemantika

data _++_↓_ : Prefroccs → Prefroccs → Prefroccs → Set where
  ε    : ∀{p} → ε ++ p ↓ p
  deci : ∀{p d} → deci d ++ p ↓ deci d + p
  plus : ∀{p₁ p₂ p₃ p}
       → p₂ ++ p₃ ↓ p
       → (p₁ + p₂) ++ p₃ ↓ p₁ + p


data _↓_,_ : Prefroccs → Prefroccs → Prefroccs → Set where
  b : deci bor ↓ deci bor + ε , ε
  s : deci szo ↓ ε , deci szo + ε
  ε : ε ↓ ε , ε
  plus : ∀{p₁ p₂ p₁₁ p₁₂ p₂₁ p₂₂ p₁' p₂'}
       → p₁ ↓ p₁₁ , p₁₂
       → p₂ ↓ p₂₁ , p₂₂
       → p₁₁ ++ p₂₁ ↓ p₁'
       → p₁₂ ++ p₂₂ ↓ p₂'
       → p₁ + p₂ ↓ p₁' , p₂'

infix 2 _↓_,_
infix 2 _++_↓_

-- normal formak

data _borrec : Prefroccs → Set where
  ε : ε borrec
  bor+ : ∀{p} → p borrec → deci bor + p borrec

infix 2 _borrec

data _szorec : Prefroccs → Set where
  ε : ε szorec
  szo+ : ∀{p} → p szorec → deci szo + p szorec

infix 2 _szorec

hal++ : ∀ p₁ p₂ → Σ Prefroccs λ p → p₁ ++ p₂ ↓ p
hal++ ε  p₂ = p₂ , ε
hal++ (deci d) p₂ = deci d + p₂ , deci
hal++ (p₁ + p₂) p₃ = p₁ + proj₁ (hal++ p₂ p₃) , plus (proj₂ (hal++ p₂ p₃))

hal : ∀ p → Σ Prefroccs λ p₁ → Σ Prefroccs λ p₂ → p ↓ p₁ , p₂ -- haladas
hal (deci bor) = deci bor + ε , (ε , b)
hal (deci szo) = ε , (deci szo + ε , s)
hal ε = ε , (ε , ε)
hal (p₁ + p₂) with hal p₁ | hal p₂
hal (p₁ + p₂) | p₁₁ , (p₁₂ , t₁) | (p₂₁ , (p₂₂ , t₂)) = p₁' , (p₂' , plus t₁ t₂ t₁' t₂')
  where
    pt₁' = hal++ p₁₁ p₂₁
    pt₂' = hal++ p₁₂ p₂₂
    p₁' = proj₁ pt₁'
    p₂' = proj₁ pt₂'
    t₁' = proj₂ pt₁'
    t₂' = proj₂ pt₂'

-- a vegen receptet kapunk

borrec++ : ∀{t₁ t₂ t₃} → t₁ borrec → t₂ borrec → t₁ ++ t₂ ↓ t₃ → t₃ borrec
borrec++ bt₁ bt₂ ε = bt₂
borrec++ () bt₂ deci
borrec++ (bor+ bt₁) ε (plus t) = bor+ (borrec++ bt₁ ε t)
borrec++ (bor+ bt₁) (bor+ bt₂) (plus t) = bor+ (borrec++ bt₁ (bor+ bt₂) t)

borrec : ∀{p p₁ p₂} → p ↓ p₁ , p₂ → p₁ borrec
borrec b = bor+ ε
borrec s = ε
borrec ε = ε
borrec {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂') with borrec t₁ | borrec t₂
... | bt₁ | bt₂ = borrec++ {p₁₁}{p₂₁}{p₁'} bt₁ bt₂ t₁'

szorec++ : ∀{t₁ t₂ t₃} → t₁ szorec → t₂ szorec → t₁ ++ t₂ ↓ t₃ → t₃ szorec
szorec++ st₁ st₂ ε = st₂
szorec++ () st₂ deci
szorec++ (szo+ st₁) ε (plus t) = szo+ (szorec++ st₁ ε t)
szorec++ (szo+ st₁) (szo+ st₂) (plus t) = szo+ (szorec++ st₁ (szo+ st₂) t)

szorec : ∀{p p₁ p₂} → p ↓ p₁ , p₂ → p₂ szorec
szorec b = ε
szorec s = szo+ ε
szorec ε = ε
szorec {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂') with szorec t₁ | szorec t₂
... | st₁ | st₂ = szorec++ {p₁₂}{p₂₂}{p₂'} st₁ st₂ t₂'

-- egyertelmuseg

egy++ : ∀{p₁ p₂ p q} → p₁ ++ p₂ ↓ p → p₁ ++ p₂ ↓ q → p ≡ q
egy++ ε ε = refl
egy++ deci deci = refl
egy++ (plus t) (plus r) = ap (_+_ _) (egy++ t r)

egy : ∀{p p₁ p₂ q₁ q₂} → p ↓ p₁ , p₂ → p ↓ q₁ , q₂ → (p₁ ≡ q₁) × (p₂ ≡ q₂)
egy b b = refl , refl
egy s s = refl , refl
egy ε ε = refl , refl
egy {.(p₁ + p₂)}{.p₁'}{.p₂'}{.q₁'}{.q₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(plus { q₁}{ q₂}{ q₁₁}{ q₁₂}{ q₂₁}{ q₂₂}{q₁'}{q₂'} r₁ r₂ r₁' r₂') with egy t₁ r₁ | egy t₂ r₂
egy {.(p₁ + p₂)}{.p₁'}{.p₂'}{.q₁'}{.q₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(plus {.p₁}{.p₂}{.p₁₁}{.p₁₂}{.p₂₁}{.p₂₂}{q₁'}{q₂'} r₁ r₂ r₁' r₂') | refl , refl | refl , refl
  = egy++ t₁' r₁' , egy++ t₂' r₂'

-- tipusmegorzes

oriz++ : ∀{p₁ p₂ p A} → p₁ ++ p₂ ↓ p → p₁ + p₂ ∶ A → p ∶ A
oriz++ ε (bs () _)
oriz++ ε (sb () _)
oriz++ ε (() +p)
oriz++ ε (p+ pA) = pA
oriz++ deci pA = pA
oriz++ {.(p₁ + p₂)}{.p₃}{.(p₁ + p)}(plus {p₁}{p₂}{p₃}{p} t)(bs (p₁B +p) p₃S) = bs p₁B (oriz++ {A = Vanszo} t (p+ p₃S))
oriz++ {.(p₁ + p₂)}{.p₃}{.(p₁ + p)}(plus {p₁}{p₂}{p₃}{p} t)(bs (p+ p₂B) p₃S) = p+ oriz++ t (bs p₂B p₃S)
oriz++ {.(p₁ + p₂)}{.p₃}{.(p₁ + p)}(plus {p₁}{p₂}{p₃}{p} t)(sb (p₁S +p) p₃B) = sb p₁S (oriz++ t (p+ p₃B))
oriz++ {.(p₁ + p₂)}{.p₃}{.(p₁ + p)}(plus {p₁}{p₂}{p₃}{p} t)(sb (p+ p₂S) p₃B) = p+ oriz++ t (sb p₂S p₃B)
oriz++ {.(p₁ + p₂)}{.p₃}{.(p₁ + p)}(plus {p₁}{p₂}{p₃}{p} t)(bs p₁B p₂S +p) = bs p₁B (oriz++ t (p₂S +p))
oriz++ {.(p₁ + p₂)}{.p₃}{.(p₁ + p)}(plus {p₁}{p₂}{p₃}{p} t)(sb p₁S p₂B +p) = sb p₁S (oriz++ t (p₂B +p))
oriz++ {.(p₁ + p₂)}{.p₃}{.(p₁ + p)}(plus {p₁}{p₂}{p₃}{p} t)((p₁A +p) +p) = p₁A +p
oriz++ {.(p₁ + p₂)}{.p₃}{.(p₁ + p)}(plus {p₁}{p₂}{p₃}{p} t)((p+ p₂A) +p) = p+ oriz++ t (p₂A +p)
oriz++ {.(p₁ + p₂)}{.p₃}{.(p₁ + p)}(plus {p₁}{p₂}{p₃}{p} t)(p+ p₃A) = p+ oriz++ t (p+ p₃A)

oriz : ∀{p p₁ p₂ A} → p ↓ p₁ , p₂ → p ∶ A → p₁ + p₂ ∶ A
oriz b b = (b +p) +p
oriz s s = p+ (s +p)
oriz ε ()
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(bs p₁B p₂S) with oriz t₁ p₁B | oriz t₂ p₂S
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(bs p₁B p₂S) | p₁₁B +p | p₂₁S +p = oriz++ t₁' (bs p₁₁B p₂₁S) +p
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(bs p₁B p₂S) | p₁₁B +p | p+ p₂₂S = bs (oriz++ t₁' (p₁₁B +p)) (oriz++ t₂' (p+ p₂₂S))
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(bs p₁B p₂S) | p+ p₁₂B | p₂₁S +p = sb (oriz++ t₁' (p+ p₂₁S)) (oriz++ t₂' (p₁₂B +p))
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(bs p₁B p₂S) | p+ p₁₂B | p+ p₂₂S = p+ oriz++ t₂' (bs p₁₂B p₂₂S)
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(sb p₁S p₂B) with oriz t₁ p₁S | oriz t₂ p₂B
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(sb p₁S p₂B) | p₁₁S +p | (p₂₁B +p) = oriz++ t₁' (sb p₁₁S p₂₁B) +p
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(sb p₁S p₂B) | p₁₁S +p | (p+ p₂₂B) = sb (oriz++ t₁' (p₁₁S +p)) (oriz++ t₂' (p+ p₂₂B))
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(sb p₁S p₂B) | p+ p₁₂S | (p₂₁B +p) = bs (oriz++ t₁' (p+ p₂₁B)) (oriz++ t₂' (p₁₂S +p))
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(sb p₁S p₂B) | p+ p₁₂S | (p+ p₂₂B) = p+ oriz++ t₂' (sb p₁₂S p₂₂B)
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(p₁A +p) with oriz t₁ p₁A
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(p₁A +p) | bs p₁₁B p₁₂S = bs (oriz++ t₁' (p₁₁B +p)) (oriz++ t₂' (p₁₂S +p))
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(p₁A +p) | sb p₁₁S p₁₂B = sb (oriz++ t₁' (p₁₁S +p)) (oriz++ t₂' (p₁₂B +p))
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(p₁A +p) | p₁₁A +p = oriz++ t₁' (p₁₁A +p) +p
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(p₁A +p) | p+ p₁₂A = p+ oriz++ t₂' (p₁₂A +p)
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(p+ p₂A) with oriz t₂ p₂A
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(p+ p₂A) | bs p₂₁B p₂₂S = bs (oriz++ t₁' (p+ p₂₁B)) (oriz++ t₂' (p+ p₂₂S))
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(p+ p₂A) | sb p₂₁S p₂₂B = sb (oriz++ t₁' (p+ p₂₁S)) (oriz++ t₂' (p+ p₂₂B))
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(p+ p₂A) | p₂₁A +p = oriz++ t₁' (p+ p₂₁A) +p
oriz {.(p₁ + p₂)}{.p₁'}{.p₂'}(plus {p₁}{p₂}{p₁₁}{p₁₂}{p₂₁}{p₂₂}{p₁'}{p₂'} t₁ t₂ t₁' t₂')(p+ p₂A) | p+ p₂₂A = p+ oriz++ t₂' (p+ p₂₂A)

⟦_⟧' : Prefroccs → Prefroccs × Prefroccs
⟦ p ⟧' with hal p
... | (p₁ , (p₂ , _)) = p₁ , p₂
